from flask import Blueprint, request

from models.model_status import Status

status = Blueprint('status_route', __name__, url_prefix = '/status')

@status.route('', methods=['POST'])
def post():
    query = request.json
    status = Status(**query)

    if Status.find_status(query['id']):
        return {'message': f"status {query['id']} already exists"}, 400
    try:
        status.save_status()
        return {'message': 'status sucessfully created'}, 201 
    except:
        return {'message': 'internal error'}, 500

@status.route('/<descricao>')
def get(descricao):
    status = Status.find_descricao(descricao)
    if status:
        return status.json(), 200
    return {'message': 'status not found'}, 400