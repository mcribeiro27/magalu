from flask import Blueprint, request
from models.model_agenda import Agendamento

agenda = Blueprint('agenda_route', __name__, url_prefix = '/agendamento')

@agenda.route('', methods=['POST'])
def post():
    query = request.json
    agenda = Agendamento(**query)

    if Agendamento.find_agenda(query['id']):
        return {'message': f"agendamento {query['id']} already exists"}, 400
    try:
        agenda.save_agenda()
        return {'message': 'agendamendo sucessfully created'}, 201 
    except:
        return {'message': 'internal error'}, 500


@agenda.route('/<int:id>', methods=['DELETE'])
def delete(id):
    agenda = Agendamento.find_agenda(id)
    if agenda:
        agenda.delete_agenda()
        return {'message': 'agendamento deleted'}, 200
    return {'message': 'agendamento not found'}, 400
