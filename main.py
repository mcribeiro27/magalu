from flask import Flask, jsonify

from views.agenda import agenda
from views.status import status

app = Flask(__name__)
app.register_blueprint(agenda)
app.register_blueprint(status)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@127.0.0.1/magalu'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


@app.before_first_request
def cria_banco():
    db.create_all()

@app.route('/')
def index():
    return jsonify({'mensage': 'bem vindo ao sistema de agendamento da Magalu'})

if __name__ == '__main__':
    from sql import db
    db.init_app(app)
    app.run(debug=True)
