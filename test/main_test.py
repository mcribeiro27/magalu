import json

import pytest
import requests
from main import app, cria_banco
from flask_sqlalchemy import SQLAlchemy


@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    sqlalchemy = SQLAlchemy()
    sqlalchemy.init_app(app)

    yield client

def test_status_post(client):
    url = 'http://127.0.0.1:5000/status'
    headers = {
        'Content-Type': 'application/json'
    }
    payload = {'id': 1, 'descricao': 'aberto'}
    rv = requests.request('POST', url, headers=headers, data=json.dumps(payload))
    assert rv.status_code == 201

def test_agenda_post(client):
    url = 'http://127.0.0.1:5000/agendamento'
    headers = {
        'Content-Type': 'application/json'
    }
    payload = {
        "id": 1,
        "data": "02/07/2020",
        "destinatario": "Diretoria",
        "mensagem": "Olá Diretoria",
        "status_id": 1
    }
    rv = requests.request('POST', url, headers=headers, data=json.dumps(payload))
    assert rv.status_code == 201

def test_status_get(client):
    url = 'http://127.0.0.1:5000/status/aberto'
    headers = {
        'Content-Type': 'application/json'
    }
    
    rv = requests.request('GET', url, headers=headers)
    assert rv.status_code == 200

def test_agenda_delete(client):
    url = 'http://127.0.0.1:5000/agendamento/1'
    headers = {
        'Content-Type': 'application/json'
    }
    
    rv = requests.request('DELETE', url, headers=headers)
    assert rv.status_code == 200
