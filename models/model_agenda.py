from datetime import datetime

from sqlalchemy.orm import backref

from sql import db


class Agendamento(db.Model):
    __tablename__ = 'agendamento'

    id = db.Column(db.Integer, primary_key = True)
    data = db.Column(db.String(50))
    destinatario = db.Column(db.String(50))
    mensagem = db.Column(db.Text)
    status_id = db.Column(db.Integer, db.ForeignKey('status.id'))
   

    def __init__(self, id, data, destinatario, mensagem, status_id):
        self.id = id
        self.data = data
        self.destinatario = destinatario
        self.mensagem = mensagem
        self.status_id = status_id
      

    def json(self):
        return {
            'id': self.id,
            'data': self.data,
            'destinatario': self.destinatario,
            'mensagem': self.mensagem,
            'status_id': self.status_id
        }

    @classmethod
    def find_agenda(cls, id):
        agenda = cls.query.filter_by(id = id).first()
        if agenda:
            return agenda
        return None

    def save_agenda(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_agenda(self):
        db.session.delete(self)
        db.session.commit()
