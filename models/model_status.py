from sql import db

class Status(db.Model):
    __tablename__ = 'status'

    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(20))
    agendamento = db.relationship('Agendamento')

    def __init__(self, id, descricao):
        self.id = id
        self.descricao = descricao

    def json(self):
        return {
            'id': self.id,
            'descricao': self.descricao,
            'agendamento': [agenda.json() for agenda in self.agendamento]
        }
        
    @classmethod
    def find_status(cls, id):
        status = cls.query.filter_by(id=id).first()
        if status:
            return status
        return None

    @classmethod
    def find_descricao(cls, descricao):
        status = cls.query.filter_by(descricao = descricao).first()
        if status:
            return status
        return None

    def save_status(self):
        db.session.add(self)
        db.session.commit()
    