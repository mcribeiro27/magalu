# Teste Magalu

## Apresentação

Documentação para utilização da Doc API - Desafio Magalu.

## Autenticação

Não é preciso utilizar autenticação para fazer requisições a esta API.

## Começando

Para acessar o sistema serão necessários os seguintes programas:

- [Python 3.7.5: necessário para a execução do sistema](https://www.python.org/downloads/)
- [Mysql Community Server](https://dev.mysql.com/downloads/mysql/)
- [Postman: necessário para teste da api](www.postman.com)

## Desenvolvimento

Para iniciar o desenvolvimento, é necessário clonar o projeto do Gitlab em um diretório de sua preferência:
```commandline
cd "diretorio de sua preferencia"
git clone git clone https://gitlab.com/mcribeiro27/magalu.git
```

## Construção

Para construir o projeto, execute os comandos abaixo dentro da pasta onde baixou o projeto:

Para ambiente Unix
```commandline
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```
Para ambiente Windows
```commandline
pip install virtualenv
virtualenv venv
venv/Scripts/activate.bat
pip install -r requirements.txt
```
O Comando irá instalar todos os módulos necessários para a execução do sistema

Precisa também criar o Schema para o banco de dados no Mysql
```commandline
CREATE SCHEMA magalu
```
As tabelas o proprio python cria
Tambem é necessário troca o usuário e senha do seu banco de dados no arquivo 'main.py'

## Configuração

### Agendamento

Através desta pasta conseguimos agendar uma comunicação na Doc Api – Desafio magalu.

### POST Novo status

```url
http://127.0.0.1:5000/status
```
Cadastra um novo status no sistema

### Body

```
{   
    "id": 1,
    "descricao": "aberto"
}
```
### POST Novo agendmento

```url
http://127.0.0.1:5000/agendamento
```
Cadastra um novo agendamento no sistema

### Body

```
  {
    "id": 5,
    "data": "02/07/2020",
    "destinatario": "Diretoria",
    "mensagem": "Olá Diretoria",
    "status_id": 1
  } 
```

### GET Lista status

```url
http://127.0.0.1:5000/status/aberto
```
Lista todos os agendamento com o status selecionado

### Response
```
{
  "agendamento": [
    {
      "id": 5,
      "data": "02/07/2020",
      "destinatario": "Diretoria",
      "mensagem": "Olá Diretoria",
      "status_id": 1
    } 
  ],
  "id": 1,
  "descricao": "aberto"
}
```

### DELETE Apaga os agendamentos

```url
http://127.0.0.1:5000/agendamento/<int:id>
```
Deleta um agendamento no sistema

### Response
```
{
  "menssage": "agendamento deleted"
}
```

## Teste

Foi realizado com a biblioteca pytest.
no terminal basta executar 

```comandline
pytest
``` 
